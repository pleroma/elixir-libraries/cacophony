defmodule Cacophony.Server do
  @moduledoc """
  A simple ranch protocol handler that services LDAP requests and dispatches
  them to a handler module.
  """
  @behaviour :ranch_protocol

  require Logger

  alias Cacophony.Message

  def start_link(ref, socket, transport, mod: mod) do
    pid = :proc_lib.spawn_link(__MODULE__, :init, [ref, socket, transport, mod])
    {:ok, pid}
  end

  def init(ref, _, transport, mod) do
    {:ok, socket} = :ranch.handshake(ref)
    :ok = transport.setopts(socket, [{:active, true}])

    Logger.debug("Opening connection: #{inspect(socket)}")

    :gen_server.enter_loop(__MODULE__, [], %{
      ref: ref,
      socket: socket,
      transport: transport,
      mod: mod
    })
  end

  def handle_info({:tcp, socket, data}, %{socket: socket, transport: transport, mod: mod} = state) do
    {:ok, %{} = msg} = Message.decode(data)

    Logger.debug("Incoming message: #{inspect(msg)} [state #{inspect(state)}]")

    case mod.handle_message(msg, state) do
      {:ok, reply, new_state} ->
        {:ok, bin} = Message.encode(reply)
        transport.send(socket, bin)
        {:noreply, new_state}

      :noreply ->
        {:noreply, state}

      {:error, e} ->
        Logger.error("While processing message #{inspect(msg)}, #{inspect(e)} occured.")
        transport.close(socket)
        {:stop, :normal, state}
    end
  end

  def handle_info({:tcp_closed, socket}, %{socket: socket, transport: transport} = state) do
    Logger.debug("Closing connection: #{inspect(socket)}")

    transport.close(socket)
    {:stop, :normal, state}
  end

  def handle_info({:tcp_error, socket}, %{socket: socket} = state) do
    Logger.debug("Closing connection: #{inspect(socket)}")

    {:stop, :normal, state}
  end
end
