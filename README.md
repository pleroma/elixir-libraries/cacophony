# Cacophony

**A programmable LDAP server implementation.**

## Why?

Many applications support LDAP-based authentication.  Cacophony provides a mechanism
to allow applications to embed a stub LDAP server which supports LDAP-based
authentication, allowing your application to provide single sign-on type functionality.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `cacophony` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:cacophony, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/cacophony](https://hexdocs.pm/cacophony).

