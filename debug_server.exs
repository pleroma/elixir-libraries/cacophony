require Logger

Application.ensure_started(:cacophony)
Application.ensure_all_started(:ranch)

alias Cacophony.Message

defmodule DebugServer do
  def handle_message(%Message.BindRequest{} = request, state) do
    bind_dn = request.dn != "" && request.dn || "cn=anonymous"
    state = put_in(state, [:dn], bind_dn)

    response = %Message.BindResponse{
      id: request.id,
      result_code: :success,
      matched_dn: state.dn
    }

    {:ok, response, state}
  end

  def handle_message(%Message.UnbindRequest{}, _state), do: :noreply

  def handle_message(%Message.WhoAmIRequest{} = request, %{dn: dn} = state) do
    "cn=" <> authzid = dn

    response = %Message.WhoAmIResponse{
      id: request.id,
      matched_dn: dn,
      result_code: :success,
      authzid: "u:#{authzid}@example.com"
    }

    {:ok, response, state}
  end

  def handle_message(%Message.SearchRequest{} = request, state) do
    response = %Message.SearchResultDone{
      id: request.id,
      matched_dn: "",
      result_code: :success
    }

    {:ok, response, state}
  end

  def handle_message(_message), do: {:error, :badmatch}
end

defmodule DebugListener do
  def start_link(_opts) do
    :ranch.start_listener(make_ref(), :ranch_tcp, [{:port, 6389}], Cacophony.Server, [mod: DebugServer])
  end

  def child_spec(_) do
    %{id: __MODULE__, start: {__MODULE__, :start_link, [[]]}}
  end
end

defmodule DebugSupervisor do
  use Supervisor

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  @impl true
  def init(:ok) do
    children = [DebugListener]

    Supervisor.init(children, strategy: :one_for_one)
  end
end

{:ok, pid} = DebugSupervisor.start_link([])
ref = Process.monitor(pid)

Logger.info("Debugging server started on port 6389.")

receive do
  {:DOWN, ^ref, _, _, _} -> Process.exit()
end
